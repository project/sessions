<?php

namespace Drupal\sessions;

use Drupal\Core\TempStore\PrivateTempStoreFactory as CorePrivateTempStoreFactory;

/**
 * Creates a PrivateTempStore object for a given collection.
 */
class PrivateTempStoreFactory extends CorePrivateTempStoreFactory {

  /**
   * Creates a PrivateTempStore.
   *
   * @param string $collection
   *   The collection name to use for this key/value store. This is typically
   *   a shared namespace or module name, e.g. 'views', 'entity', etc.
   *
   * @return \Drupal\Core\TempStore\PrivateTempStore
   *   An instance of the key/value store.
   */
  public function get($collection) {
    // Store the data for this collection in the database.
    $storage = $this->storageFactory->get("user.tempstore.private.$collection");
    return new PrivateTempStore($storage, $this->lockBackend, $this->currentUser, $this->requestStack, $this->expire);
  }

}
