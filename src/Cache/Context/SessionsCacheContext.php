<?php

namespace Drupal\sessions\Cache\Context;

use function array_shift;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\sessions\CurrentSessionEntity;

/**
 * Cache context ID: 'session_entity'.
 */
class SessionsCacheContext implements CalculatedCacheContextInterface {


  protected $currentSession;


  public function __construct(CurrentSessionEntity $current_session) {
    $this->currentSession = $current_session;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Sessions');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($reference = NULL) {
    $ref = explode('.', $reference);
    $id = array_shift($ref);

    $session = $this->currentSession->getCurrentUserSessionEntity($id);
    $value = ['sessions' => TRUE];
    if (!empty($session)) {
      $value = [];
      if (!empty($ref)) {
        foreach ($ref as $field) {
          if (!empty($field)) {
            $value[$field] = $session->get($field)->getValue();
          }
        }
      }
      else {
        $value = $session->toArray();
      }
    }

    return base64_encode(serialize($value));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($reference = NULL) {
    if (!$reference) {
      throw new \LogicException('No field name provided for session_entity cache context.');
    }
    $cacheable_metadata = new CacheableMetadata();
    return $cacheable_metadata->setCacheTags(["sessions.$reference"]);
  }

}
