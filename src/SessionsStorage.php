<?php

namespace Drupal\sessions;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\ContentEntityNullStorage;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use function ini_get;
use function str_replace;

/**
 * Session entity storage on steroids.
 */
class SessionsStorage extends ContentEntityNullStorage {

  protected $eventDispatcher;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a SessionStorage object.
   */
  public function __construct(EntityTypeInterface $entity_type,
                              EntityFieldManagerInterface $entity_field_manager,
                              CacheBackendInterface $cache,
                              MemoryCacheInterface $memory_cache = NULL,
                              EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
                              SessionManagerInterface $session_manager,
                              AccountInterface $current_user,
                              EventDispatcherInterface $event_dispatcher,
                              RequestStack $request_stack,
                              PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($entity_type, $entity_field_manager, $cache, $memory_cache, $entity_type_bundle_info);

    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;
    $this->eventDispatcher = $event_dispatcher;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_field.manager'),
      $container->get('cache.entity'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('session_manager'),
      $container->get('current_user'),
      $container->get('event_dispatcher'),
      $container->get('request_stack'),
      $container->get('sessions.tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSessionsTempStore() {
    return $this->tempStoreFactory->get('sessions');
  }

  /**
   * {@inheritdoc}
   */
  public function load($id) {
    $sessions = &drupal_static('sessions_storage', []);
    if (empty($sessions[$id])) {
      // Load the entity from the user's private tempstore.
      $per_user_private_temp_store = $this->tempStoreFactory->get('sessions');
      $entity = $per_user_private_temp_store->get($id);
    }
    else {
      $entity = $sessions[$id];
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    if ($this->currentUser->isAnonymous()) {
      // Force a session to be started for anonymous users. Workaround for a
      // core bug.
      // @TODO: remove this when https://www.drupal.org/node/2743931 is fixed.
      $_SESSION['session_entity_forced'] = '';
    }

    // @TODO This is really necessary?
    // Execute pre-save logic and invoke the related hooks.
    if (isset($entity->original)) {
      unset($entity->original);
    }

    // $this->doPreSave($entity);
    // Save the entity in request drupal_static.
    $id = $entity->getType();
    $saved = &drupal_static('sessions_storage_saved');
    $sessions = &drupal_static('sessions_storage');

    if ($this->needFlushMemory()) {
      // Save the entity to the user's private tempstore.
      $per_user_private_temp_store = $this->tempStoreFactory->get('sessions');
      $per_user_private_temp_store->set($entity->getType(), $entity);
      $sessions[$id] = NULL;
      $saved[$id] = NULL;
      $memory = memory_get_usage(TRUE);
      return;
    }

    $id = $entity->getType();
    $entity->enforceSave(FALSE);
    $this->tempStoreFactory->get('sessions')->set($id, $entity);
    return;

    // Save the entity in drupal_static for save when finish the request.
    $saved = &drupal_static('sessions_storage_saved');
    $saved[$id] = $this->getSessionsTempStore()->getOwnerId();
    $sessions = &drupal_static('sessions_storage');
    $entity->__sleep();
    $sessions[$id] = $entity;
  }

  /**
   *
   */
  public function needFlushMemory() {
    $memory_limit = $this->returnBytes(ini_get('memory_limit'));
    $top_limit = $memory_limit - (50 * 1024 * 1024);
    $memory = memory_get_usage(TRUE);
    if ($memory >= $top_limit) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Converts shorthand memory notation value to bytes
   * From http://php.net/manual/en/function.ini-get.php.
   *
   * @param $val
   *   Memory size shorthand notation string
   */
  public function returnBytes($val) {
    $val = trim($val);
    $last = $val[strlen($val) - 1];
    $num = str_replace($last, '', $val);
    $last = strtolower($last);
    switch ($last) {
      // The 'G' modifier is available since PHP 5.1.0.
      case 'g':
        $num *= 1024;
      case 'm':
        $num *= 1024;
      case 'k':
        $num *= 1024;
    }
    return $num;
  }

  /**
   *
   */
  public function persist(EntityInterface $entity, $sess_id) {
    if ($this->currentUser->isAnonymous()) {
      // Force a session to be started for anonymous users. Workaround for a
      // core bug.
      // @TODO: remove this when https://www.drupal.org/node/2743931 is fixed.
      $_SESSION['session_entity_forced'] = '';
    }
    // Save the entity to the user's private tempstore when KernelEvents::TERMINATE.
    $per_user_private_temp_store = $this->tempStoreFactory->get('sessions');
    $per_user_private_temp_store->set($entity->getType(), $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    $session = parent::create($values);
    $expire = REQUEST_TIME + $session->getExpire();
    $session->setExpirationDate($expire);
    return $session;
  }

  /**
   * {@inheritdoc}
   */
  protected function doPreSave(EntityInterface $entity) {
    $id = $entity->id();

    // Track the original ID.
    if ($entity->getOriginalId() !== NULL) {
      $id = $entity->getOriginalId();
    }

    // Track if this entity exists already.
    $id_exists = $this->has($id, $entity);

    // A new entity should not already exist.
    if ($id_exists && $entity->isNew()) {
      throw new EntityStorageException("'{$this->entityTypeId}' entity with ID '$id' already exists.");
    }

    // Load the original entity, if any.
    if (!isset($entity->original)) {
      $original = $this->loadUnchanged($id);
      $entity->original = $original;
    }

    // Allow code to run before saving.
    $entity->preSave($this);

    // $this->eventDispatcher->dispatch(SessionEvents::PRESAVE, new SessionEvent($entity, $entity->original));.
    return $id;
  }

}
