<?php

namespace Drupal\sessions\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sessions\PrivateTempStore;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Saves persistently all de sessions entities that has been saved in the
 * request.
 */
class SessionsRequestSubscriber implements EventSubscriberInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  protected $perUserPrivateTempStore;

  /**
   * Constructs a new SessionsRequestSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, KeyValueExpirableFactoryInterface $storage_factory, LockBackendInterface $lock_backend, AccountProxyInterface $current_user, RequestStack $request_stack, $expire = 604800) {
    $storage = $storage_factory->get("user.tempstore.private.sessions");
    $this->entityTypeManager = $entity_manager;
    $this->perUserPrivateTempStore = new PrivateTempStore($storage, $lock_backend, $current_user, $request_stack, $expire);
  }

  /**
   * Saves persistently all de sessions entities that has been saved in the
   * request.
   *
   * @param \Symfony\Component\HttpKernel\Event\PostResponseEvent $event
   *   The event to process.
   */
  public function onKernelTerminate(PostResponseEvent $event) {
    /** @var \Drupal\sessions\SessionsStorage $storage */
    $storage = $this->entityTypeManager->getStorage('sessions');
    $saved = drupal_static('sessions_storage_saved', []);
    foreach ($saved as $id => $sess_id) {
      $session = $storage->load($id);
      //$storage->persist($session, $sess_id);
      $expire = $session->getExpire();
      $this->perUserPrivateTempStore->setExpire($expire);
      $this->perUserPrivateTempStore->set($session->getType(), $session, $sess_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['onKernelTerminate', 10000];
    return $events;
  }

}
