<?php

namespace Drupal\sessions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SessionsTypeForm.
 *
 * @package Drupal\sessions\Form
 */
class SessionsTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $sessions_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $sessions_type->label(),
      '#description' => $this->t("Label for the Sessions type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $sessions_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\sessions\Entity\SessionsType::load',
      ],
      '#disabled' => !$sessions_type->isNew(),
    ];

    $form['pattern'] = array(
      '#type' => 'textfield',
      '#title' => 'Pattern',
      '#default_value' => $sessions_type->get('pattern'),
      '#size' => 65,
      '#maxlength' => 1280,
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => ['sessions'],
      '#min_tokens' => 1
    );

    // Show the token help relevant to this pattern type.
    $form['token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => ['sessions'],
    );

    $expire = \Drupal::getContainer()->getParameter('tempstore.expire');
    $form['tempstore_expire'] = array(
      '#type' => 'number',
      '#title' => $this->t('Tempstore expire'),
      '#description' => $this->t('Tempstore expire time in seconds'),
      '#default_value' => !empty($sessions_type->get('tempstore_expire')) ? $sessions_type->get('tempstore_expire') : $expire,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $sessions_type = $this->entity;
    $status = $sessions_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Sessions type.', [
          '%label' => $sessions_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Sessions type.', [
          '%label' => $sessions_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($sessions_type->toUrl('collection'));
  }

}
