<?php

namespace Drupal\sessions;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service to get the current user's session entity.
 */
class CurrentSessionEntity {

  /**
   * The entity storage for session entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|object
   */
  protected $sessionEntityStorage;

  /**
   * Constructs a new instance of the CurrentSessionEntity.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->sessionEntityStorage = $entity_type_manager->getStorage('sessions');
  }

  /**
   * Get the session entity for the current user.
   *
   * @param string $bundle
   *   The session bundle.
   *
   * @return \Drupal\sessions\Entity\Sessions|null
   *   The session entity for the current user, or NULL if the current user does
   *   not have one.
   */
  public function getCurrentUserSessionEntity($bundle) {
    return $this->sessionEntityStorage->load($bundle);
  }

}
