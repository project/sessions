<?php

namespace Drupal\sessions;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\TempStore\PrivateTempStore as CorePrivateTempStore;
use Drupal\Core\TempStore\TempStoreException;
use function method_exists;
use const REQUEST_TIME;

/**
 * {@inheritdoc}
 */
class PrivateTempStore extends CorePrivateTempStore {

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    $key = $this->createkey($key);
    if (($object = $this->storage->get($key)) && ($object->owner == $this->getOwner())) {
      if (!empty($object->expire) && method_exists($object->data, 'setExpirationDate')) {
        $object->data->setExpirationDate($object->expire);
      }
      return $object->data;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value, $sess_id = NULL) {
    if (!$sess_id) {
      if ($this->currentUser->isAnonymous()) {
        // Ensure that an anonymous user has a session created for them, as
        // otherwise subsequent page loads will not be able to retrieve their
        // tempstore data. Note this has to be done before the key is created as
        // the owner is used in key creation.
        $this->startSession();
        $session = $this->requestStack->getCurrentRequest()->getSession();
        if (!$session->has('core.tempstore.private.owner')) {
          $session->set('core.tempstore.private.owner', Crypt::randomBytesBase64());
        }
      }
      $key = $this->createkey($key);
      $owner = $this->getOwner();
    }
    else {
      $key = $sess_id . ':' . $key;
      $owner = $sess_id;
    }
    if (!$this->lockBackend->acquire($key)) {
      $this->lockBackend->wait($key);
      if (!$this->lockBackend->acquire($key)) {
        throw new TempStoreException("Couldn't acquire lock to update item '$key' in '{$this->storage->getCollectionName()}' temporary storage.");
      }
    }

    $value = (object) [
      'owner' => $owner,
      'data' => $value,
      'updated' => (int) $this->requestStack->getMasterRequest()->server->get('REQUEST_TIME'),
      'expire' => REQUEST_TIME + $this->expire,
    ];
    $this->storage->setWithExpire($key, $value, $this->expire);
    $this->lockBackend->release($key);
  }

  /**
   * Gets the current owner based on the current user or the session ID.
   *
   * @return string
   *   The owner.
   */
  public function getOwnerId() {
    return $this->getOwner();
  }

  /**
   * Set tempstore expiration time.
   *
   * @param int $expire
   *   Expiration time.
   */
  public function setExpire(int $expire) {
    $this->expire = $expire;
  }

}
