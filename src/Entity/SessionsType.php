<?php

namespace Drupal\sessions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Sessions type entity.
 *
 * @ConfigEntityType(
 *   id = "sessions_type",
 *   label = @Translation("Sessions type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sessions\SessionsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\sessions\Form\SessionsTypeForm",
 *       "edit" = "Drupal\sessions\Form\SessionsTypeForm",
 *       "delete" = "Drupal\sessions\Form\SessionsTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sessions\SessionsTypeHtmlRouteProvider",
 *     },
 *   },
 *   translatable = FALSE,
 *   config_prefix = "sessions_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "sessions",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/sessions_type/{sessions_type}",
 *     "add-form" = "/admin/structure/sessions_type/add",
 *     "edit-form" = "/admin/structure/sessions_type/{sessions_type}/edit",
 *     "delete-form" = "/admin/structure/sessions_type/{sessions_type}/delete",
 *     "collection" = "/admin/structure/sessions_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "pattern",
 *     "tempstore_expire"
 *   }
 * )
 */
class SessionsType extends ConfigEntityBundleBase implements SessionsTypeInterface {

  /**
   * The Sessions type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Sessions type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Sessions type pattern.
   *
   * @var string
   */
  protected $pattern;

  /**
   * The Sessions bundle tempstore expiration time.
   *
   * @var int
   */
  protected $expire;

}
