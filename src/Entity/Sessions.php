<?php

namespace Drupal\sessions\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the Sessions entity.
 *
 * @ingroup sessions
 *
 * @ContentEntityType(
 *   id = "sessions",
 *   label = @Translation("Sessions"),
 *   bundle_label = @Translation("Sessions type"),
 *   handlers = {
 *     "storage" = "Drupal\sessions\SessionsStorage",
 *     "form" = {
 *       "default" = "Drupal\sessions\Form\SessionsForm",
 *       "edit" = "Drupal\sessions\Form\SessionsForm",
 *     }
 *   },
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uid" = "user_id"
 *   },
 *   links = {
 *     "canonical" = "/sessions/{sessions}",
 *     "edit-form" = "/sessions/{sessions}/edit"
 *   },
 *   bundle_entity_type = "sessions_type",
 *   field_ui_base_route = "entity.sessions_type.edit_form"
 * )
 */
class Sessions extends ContentEntityBase {

  protected $expirationDate;

  protected $enforceSave = FALSE;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->getType();
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    $result = &drupal_static(__FUNCTION__, NULL);
    $bundle = $this->bundle();
    $session_type = $this->get('type')->entity;
    $token = \Drupal::service('token');
    $pattern = $session_type->get('pattern');
    if (!empty($pattern)) {
      $replacement = $token->replace($pattern, ['sessions' => $this]);
      $result = $bundle . '_' . $replacement;
    }
    else {
      $result = $bundle;
    }

    return $result;
  }

  public function getExpire() {
    $bundle = $this->bundle();
    $session_type = \Drupal::entityTypeManager()
      ->getStorage('sessions_type')
      ->load($bundle);
    $expire = $session_type->get('tempstore_expire');
    $default_expire = \Drupal::getContainer()->getParameter('tempstore.expire');
    return !empty($expire) ? $expire : $default_expire;
  }

  public function getExpirationDate() {
    return $this->expirationDate;
  }

  public function setExpirationDate($expiration) {
    $this->expirationDate = $expiration;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    // @todo Add bundle-specific listing cache tag?
    //   https://www.drupal.org/node/2145751
    if ($this->isNew()) {
      return [];
    }
    return [$this->entityTypeId . ':' . $this->bundle() . ':' . $this->getSessionId()];
  }

  /**
   * {@inheritdoc}
   */
  public function isNew() {
    return !empty($this->enforceIsNew) ? TRUE : FALSE;
  }

  public function getSessionId() {
    return $this->entityTypeManager()
      ->getStorage($this->entityTypeId)
      ->getSessionsTempStore()
      ->getOwnerId();
  }

  public function enforceSave($value = TRUE) {
    $this->enforceSave = $value;
  }

  public function needsSaveNow() {
    return $this->enforceSave;
  }

}
