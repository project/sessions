<?php

namespace Drupal\sessions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Sessions type entities.
 */
interface SessionsTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
